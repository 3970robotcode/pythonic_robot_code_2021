from .constants import Constants as c
import wpilib as w
import ctre

import time
import math

class Drivetrain(): 
    """Drive Train"""
   
    def __init__(self):
        """Motor And Sensor Initialization"""
        self.r_master = ctre.WPI_TalonFX(c.dt_r_master)
        self.r_slave = ctre.WPI_TalonFX(c.dt_r_slave)
        
        self.l_master = ctre.WPI_TalonFX(c.dt_l_master)
        self.l_slave = ctre.WPI_TalonFX(c.dt_l_slave)
        
        self.r_slave.set(ctre.ControlMode.Follower, c.dt_r_master)
        self.l_slave.set(ctre.ControlMode.Follower, c.dt_l_master)

        self.gyro = w.ADXRS450_Gyro()

        self.l_master.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)
        self.l_slave.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)
        self.r_master.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)
        self.r_slave.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)

        self.l_master.config_kF(0, c.drivetrain_PID["lkF"])
        self.l_master.config_kP(0, c.drivetrain_PID["lkP"])
        self.l_master.config_kI(0, c.drivetrain_PID["lkI"])
        self.l_master.config_kD(0, c.drivetrain_PID["lkD"])

        self.r_master.config_kF(0, c.drivetrain_PID["rkF"])
        self.r_master.config_kP(0, c.drivetrain_PID["rkP"])
        self.r_master.config_kI(0, c.drivetrain_PID["rkI"])
        self.r_master.config_kD(0, c.drivetrain_PID["rkD"])
        
        self.r_master.configPeakOutputForward(1.0)
        self.r_master.configPeakOutputReverse(-1.0)
        
        self.l_master.configPeakOutputForward(1.0)
        self.l_master.configPeakOutputReverse(-1.0)
        
        self.l_slave.configPeakOutputReverse(-1.0)
        self.l_slave.configPeakOutputForward(1.0)
        
        self.r_slave.configPeakOutputReverse(-1.0)
        self.r_slave.configPeakOutputForward(1.0)

        self.r_master.configClosedLoopPeakOutput(0, 1, c.timeoutMS)
        self.l_master.configClosedLoopPeakOutput(0, 1, c.timeoutMS)

        self.l_master.configVoltageCompSaturation(11.0)
        self.r_master.configVoltageCompSaturation(11.0)
        
        self.r_slave.configVoltageCompSaturation(11.0)
        self.l_slave.configVoltageCompSaturation(11.0)
         
        #self.r_slave.set(ctre.ControlMode.Follower, c.dt_r_master)
        #self.l_slave.set(ctre.ControlMode.Follower, c.dt_l_master)
        
        self.l_master.setNeutralMode(ctre.NeutralMode.Coast)
        self.l_slave.setNeutralMode(ctre.NeutralMode.Coast)
       
        self.r_master.setNeutralMode(ctre.NeutralMode.Coast)
        self.r_slave.setNeutralMode(ctre.NeutralMode.Coast)
        
        self.drive_shift = w.DoubleSolenoid(3, 4)

        self.drive_shift.set(w.DoubleSolenoid.Value.kReverse)
        
        self.isForward = True;
        
        self.held = False
        
        self.previousError = 0

    def set_output(self, l_speed: float, r_speed: float):
        self.l_master.set(ctre.ControlMode.PercentOutput, l_speed)
        self.r_master.set(ctre.ControlMode.PercentOutput, -r_speed)

    def shift(self, shift: bool=False):
        if shift and not self.held:
            print("testing")
            
            if self.isForward:
                self.drive_shift.set(w.DoubleSolenoid.Value.kReverse)
                self.isForward = False

            elif not self.isForward:
                self.driveShift.set(w.DoubleSolenoid.Value.kForward)
                self.isForward = True

            self.held = True

        elif not shift:
            self.held = False
    
    def stop_all_motors(self): 
        self.set_output(0,0)
        return True
 
    def print_motor_outputs(self):
        print("l_master Output" + str(self.l_master.getMotorOutputPercent()))
        print("r_master Output" + str(self.r_master.getMotorOutputPercent()))

    def encoder_and_gyro_reset(self):
        self.l_master.setSelectedSensorPosition(0, 0, c.timeoutMS)
        self.r_master.setSelectedSensorPosition(0, 0, c.timeoutMS)
        self.gyro.reset()
        self.previous_error = 0

    def drive_to_postion(self, inches: int):
        rotations = inches*(math.pi * 6)
        target_ticks = rotations * 30675 # 13272 high gear
        
        self.l_master.set(ctre.ControlMode.Position, -target_ticks)
        # self.l_slave.set(ControlMode.Position, -target_ticks)
        self.r_master.set(ctre.ControlMode.Position, target_ticks)
        # self.r_slave.set(ControlMode.Position, target_ticks)
        
        # print('targetDistance: ' + str(target_ticks))
        error = target_ticks - self.r_master.getSelectedSensorPosition()
        w.SmartDashboard.putNumber("rDrive Position", self.r_master.getSelectedSensorPosition())
        w.SmartDashboard.putNumber("rDrive Built In Error", self.r_master.getClosedLoopError())
        w.SmartDashboard.putNumber("rDrive Error", error)

        # print('error 1: ' + str(self.r_master.getClosedLoopError()))
        # print('error: ' + str(error))

        if (abs(error) <= 700):
            self.r_master.set(ctre.ControlMode.PercentOutput, 0)
            self.l_master.set(ctre.ControlMode.PercentOutput, 0)
            return True
        else:
            return False 
