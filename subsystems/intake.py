from .constants import Constants as c
import ctre
import wpilib as w

class Intake:
    def __init__(self):
        self.motor = ctre.WPI_VictorSPX(c.intake)
        self.intake_solenoid = w.DoubleSolenoid(c.arms_up, c.arms_down)
        
    def raise_and_lower(self, raiseArms: bool=False, lowerArms: bool=False): 
        if raiseArms:
            self.intake_solenoid.set(w.DoubleSolenoid.Value.kReverse)

        elif lowerArms:
            self.intake_solenoid.set(w.DoubleSolenoid.Value.kForward)
