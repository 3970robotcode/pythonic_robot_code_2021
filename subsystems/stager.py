import ctre

from . intake import Intake

from ctre import ControlMode
import wpilib as w
from .constants import Constants as c


class Stager:
    def __init__(self, intake: Intake):
        
        self.intake = intake;
        
        self.pre_stager_solenoid = w.Solenoid(c.pre_stager_solenoid);
        
        self.feeder = ctre.WPI_VictorSPX(c.feeder);
        
        # We should go through and set all the negative motors inverted so they all read positive
        self.pre_stager = ctre.WPI_VictorSPX(c.pre_stager_solenoid);
        self.stager = ctre.WPI_TalonSRX(c.stager);
        
        self.move_complete_1 = True;
        self.move_complete_2 = True;
        self.move_complete_3 = True;
        
        self.feed = False;
        
        self.stager.configSelectedFeedbackSensor(ctre.FeedbackDevice.QuadEncoder, 0, 20);
        self.stager.config_kF(0, c.stager_kF);
        self.stager.config_kP(0, c.stager_kP);
        self.stager.config_kI(0, c.stager_kI);
        self.stager.config_kD(0, c.stager_kD);
        self.stager.setSelectedSensorPosition(0);
        self.stager.setSensorPhase(True);

        self.position1 = w.DigitalInput(c.sensor_1);
        #position2 = new DigitalInput(5);
        #position3 = new DigitalInput(2);
        self.position4 = w.DigitalInput(c.sensor_4);
        self.position5 = w.DigitalInput(c.sensor_5);

    def main(self, intake, outake, push, shoot):
        w.SmartDashboard.putNumber("Stager Encoder", self.stager.getSelectedSensorPosition());

        if outake:
            self.pre_stager.set(ControlMode.PercentOutput, 0.75);
            self.intake.motor.set(ControlMode.PercentOutput,0.75);
            self.stager.set(ControlMode.PercentOutput, -0.5);
            self.feeder.set(ControlMode.PercentOutput, -0.75);
            self.stager.setSelectedSensorPosition(0);

        elif push:
            self.intake.motor.set(ControlMode.PercentOutput, 0.75);

        elif intake:
            w.SmartDashboard.putNumber("Stager Encoder", self.stager.getSelectedSensorPosition()); #Get rid of this line, it's a duplicate from two if statements up

            if(not self.position5.get() and not self.position1.get() and not self.position4.get()): 
                self.pre_stager.set(ControlMode.PercentOutput, 0);
                self.intake.motor.set(ControlMode.PercentOutput, -.75);
                self.pre_stager.set(ControlMode.PercentOutput, 0);
                #self.intake.motor.set(ControlMode.PercentOutput,0);
                self.feeder.set(ControlMode.PercentOutput, 0); #change this to 0.3, which stops the next ball from pushing it up
                self.stager.set(ControlMode.PercentOutput, 0);
            
            else:
                self.pre_stager.set(ControlMode.PercentOutput, -.5);
                self.intake.motor.set(ControlMode.PercentOutput, -1.00);
            
            if(not self.move_complete_3):
                if(not self.position4.get() and not self.position5.get()):
                    self.stager.set(ControlMode.PercentOutput, 0);
                
                else:
                    self.stager.set(ctre.ControlMode.Position, 3000);
                    self.feeder.set(ControlMode.PercentOutput, 0);
                    
                    if(self.stager.getSelectedSensorPosition() > 2900):
                        self.move_complete_3 = True;
                    
                    else:
                        self.move_complete_3 = False;
                        
            if(not self.position5.get()):
                self.feeder.set(ControlMode.PercentOutput, .03);
            
            if(not self.position4.get()):
                if (self.position5.get()): 
                    self.feeder.set(ctre.ControlMode.PercentOutput, -.25);
                
                else:
                    self.feeder.set(ctre.ControlMode.PercentOutput, .03);

            if(not self.position1.get()):
                self.stager.setSelectedSensorPosition(0);
                self.move_complete_3 = False;
            
        elif not shoot:
            self.intake.motor.set(ControlMode.PercentOutput, 0);
            self.pre_stager.set(ControlMode.PercentOutput, 0);
            self.stager.set(ControlMode.PercentOutput, 0);
            self.feeder.set(ControlMode.PercentOutput, 0);
                
    def pre_stager_up(self):
        self.pre_stager_solenoid.set(True)
