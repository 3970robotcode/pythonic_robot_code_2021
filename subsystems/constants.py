class Constants():
    # Define Constants here
    # I.E. Motor S, Button S, P Tuning Values, etc

    # Motor Ids

    # Drivetrain
    dt_r_master = 0
    dt_r_slave = 1

    dt_l_master = 2
    dt_l_slave = 3

    # Shooter

    # Spark Ids
    shooter_r = 4
    shooter_l = 5
    # TalonSRX
    shooter_tilt = 11
    shooter_pan = 10

    # ColorWheel
    colorwheel = 19

    # Intake
    # VictorSPX
    intake = 2

    # Stager
    # VictorSPX
    pre_stager = 0
    feeder = 1
    # VictorSPX
    # TalonSRX
    stager = 7

    # Solenoids
    # Drivetrain
    dt_out = 4
    dt_in = 3
    # Prestager
    pre_stager_solenoid = 0
    # Intake Arms
    arms_up = 2
    arms_down = 1

    # Controllers
    l_stick = 0
    r_stick = 1
    gamepad = 2

    # Pid Loops
    timeoutMS = 10

    drivetrain_PID = {
        "lkF": 0.0,
        "lkP": 0.13,
        "lkI": 0.0,
        "lkD": 1.38,

        "rkF": 0.0,
        "rkP": 0.063,
        "rkI": 0.0,
        "rkD": 0.63
    }

    shooter_PID = {
        "rkF": 0.0005,  # 0.000195
        "rkP": 0.02,  # 0.001000#0.000252#8e-3,9.350e-5
        "rkI": 0.2,  # 0.000001 # 0.000001 # 0.000002, 1.61e-6
        "rkD": 2.00,  # 1.500000 # 0.002500 # 0.009, 9.650e-5
        "rkIZone": 100,

        "lkF": 0.000195,  # 0.000195
        "lkP": 0.001,  # 0.001000 # 0.000252 # 8e-3,9.350e-5
        "lkI": 0.000001,  # 0.000001 # 0.000001 #0.000002,1.61e-6
        "lkD": 0.001,  # 1.500000 # 0.002500 # 0.009,9.650e-5
        "lkIZone": 10
    }

    # Stager P
    stager_kF = 0.0
    stager_kP = 0.50
    stager_kI = 0.0
    stager_kD = 0.0

    # Pan PID (two slots)
    # Slot 0
    pan_kF = 0.0
    pan_kP = 0.13
    pan_kI = 0.0
    pan_kD = 1.55
    # Slot 1
    pan_kF_1 = 0.0
    pan_kP_1 = .22
    pan_kI_1 = 0.000001
    pan_kD_1 = 1.55

    # Hood Angle
    tilt_kF = 0.0
    tilt_kP = .22
    tilt_kI = 0.000001
    tilt_kD = 1.55

    # DIO Sensor IDs
    sensor_1 = 1
    sensor_4 = 4
    sensor_5 = 5
    sensor_4dot5 = 0

    # Gamepad buttons
    gp = {
        "r_stick_x": 4, "r_stick_y": 5, "l_stick_x": 0, "l_stick_y": 1,
        "r_trigger": 3, "l_trigger": 2, "x": 3, "a": 1,
        "y": 4, "b": 2, "back": 7, "start": 8,
        "r_bumper": 6, "l_bumper": 5, "l_stick": 9, "r_stick": 10}
