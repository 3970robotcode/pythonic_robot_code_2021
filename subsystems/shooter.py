from .constants import Constants as c

import rev
import ctre
from ctre import ControlMode, FeedbackDevice

import wpilib as w
from wpilib import SmartDashboard

import networktables as nt

from .stager import Stager

import math


class Shooter():
    """Shooter"""

    def __init__(self, stager: Stager):

        self.stager = stager

        # self.r_motor = rev.CANSparkMax(c.shooter_r, rev.MotorType.kBrushless)
        # self.l_motor = rev.CANSparkMax(c.shooter_l, rev.MotorType.kBrushless)

        self.hood_piston = w.DoubleSolenoid(5, 6)

        self.master = ctre.WPI_TalonFX(c.shooter_r)
        self.slave = ctre.WPI_TalonFX(c.shooter_l)

        self.master.configVoltageCompSaturation(11)
        self.slave.configVoltageCompSaturation(11)

        self.slave.setInverted(True)

        self.slave.set(ControlMode.Follower, c.shooter_r)

        self.master.config_kF(0, c.shooter_PID["rkF"])
        self.master.config_kP(0, c.shooter_PID["rkP"])
        self.master.config_kI(0, c.shooter_PID["rkI"])
        self.master.config_kD(0, c.shooter_PID["rkD"])

        self.pan = ctre.WPI_TalonSRX(c.shooter_pan)
        self.hood_angle = ctre.WPI_TalonSRX(c.shooter_tilt)

        # self.r_motor.setPeriodicFramePeriod(rev.CANSparkMax.PeriodicFrame.kStatus1, 20)
        # self.l_motor.setPeriodicFramePeriod(rev.CANSparkMax.PeriodicFrame.kStatus1, 20)

        # self.r_motor.setInverted(False)
        # self.l_motor.setInverted(True)

        # self.l_Encoder = self.l_motor.getEncoder()
        # self.r_Encoder = self.r_motor.getEncoder()
        """ 
        self.r_PID = self.r_motor.getPIDController()
        self.r_PID.setP(c.shooterPID["rkP"])
        self.r_PID.setI(c.shooterPID["rkI"])
        self.r_PID.setD(c.shooterPID["rkD"])
        self.r_PID.setFF(c.shooterPID["rkF"])
        self.r_PID.setIZone(c.shooterPID["rkIZone"])

        self.l_PID = self.l_motor.getPIDController()
        self.l_PID.setP(c.shooterPID["lkP"])
        self.l_PID.setI(c.shooterPID["lkI"])
        self.l_PID.setD(c.shooterPID["lkD"]) 
        self.l_PID.setFF(c.shooterPID["lkF"]) 
        self.l_PID.setIZone(c.shooterPID["lkIZone"])
        """
        self.pan.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 20)
        self.pan.setSensorPhase(True)

        self.pan.config_kF(0, c.pan_kF)
        self.pan.config_kP(0, c.pan_kP)
        self.pan.config_kI(0, c.pan_kI)
        self.pan.config_kD(0, c.pan_kD)

        self.pan.config_kF(1, c.pan_kF_1)
        self.pan.config_kP(1, c.pan_kP_1)
        self.pan.config_kI(1, c.pan_kI_1)
        self.pan.config_kD(1, c.pan_kD_1)

        self.pan.configPeakOutputForward(1)
        self.pan.configPeakOutputReverse(-1)

        self.hood_angle.config_kF(0, c.tilt_kF)
        self.hood_angle.config_kP(0, c.tilt_kP)
        self.hood_angle.config_kI(0, c.tilt_kI)
        self.hood_angle.config_kD(0, c.tilt_kD)

        self.pan.setSelectedSensorPosition(0)
        self.hood_angle.setSelectedSensorPosition(0)

        self.position_4dot5 = w.DigitalInput(c.sensor_4dot5)

        self.move_complete_1 = True
        self.move_complete_2 = True

        self.velocity_toggle = False
        self.delay_toggle = False

        self.time_count = 0
        self.autoDelayCount = 0

        self.setpoint = 5000

        self.max_turret_setpoint = -27500
        self.min_turret_setpoint = 3000

        self.limelight = nt.NetworkTablesInstance.getDefault().getTable("limelight")

    def main(self, shoot: bool, hold: bool = False, off_time: int = 50, on_time: int = 200):
        """Main shooting function"""
        if shoot:
            # adds 20 to the time count (20 represents the 20ms loop time)
            self.time_count += 20

            # sets shooter velocity to the setpoint, and the hood to hold the initial angle
            self.set_velocity(self.setpoint)
            # self.set_hood_angle(0)

            # checks if shooter is within 5rpm of the target velocity
            if self.r_Encoder.getVelocity() >= self.setpoint - 5:
                self.velocity_toggle = True

            # compares the time count to the off time if the delay_toggle is false
            if self.time_count >= off_time and not self.delay_toggle:
                self.time_count = 0
                self.delay_toggle = True
            # compares the time count to the on time if the delay toggle is true
            elif self.time_count >= on_time and self.delay_toggle:
                self.time_count = 0
                self.delay_toggle = False

            # advances stager until a ball is ready to fire (only occurs if less than four balls are in the system)
            if self.position_4dot5.get():
                print("yes")
                self.stager.feeder.set(ControlMode.PercentOutput, .75)
                self.stager.stager.set(ControlMode.PercentOutput, .75)
                self.stager.pre_stager.set(ControlMode.PercentOutput, -.75)
            
            # checks hold, delay and velocity toggles, if the velocity is good, the delay toggle is true, and hold is false, it
            elif not hold and self.delay_toggle and self.velocity_toggle:
                print("no")
                # shoot_toggle = True
                self.stager.feeder.set(ControlMode.PercentOutput, 1)
                self.stager.stager.set(ControlMode.PercentOutput, 1)
                self.stager.pre_stager.set(ControlMode.PercentOutput, 1)
            else:
                self.stager.feeder.set(ControlMode.PercentOutput, 0.0)
                self.stager.stager.set(ControlMode.PercentOutput, 0.0)
                self.stager.pre_stager.set(ControlMode.PercentOutput, 0.0)
            # I dont think this is neccessary, will test when I get the chance (goes above the else statement
            """
            elif not self.position_4dot5.get():
                self.stager.feederMotor.set(ControlMode.PercentOutput, 0.0)
                self.stager.stager.set(ControlMode.PercentOutput, 0.0)
                self.stager.pre_stager.set(ControlMode.PercentOutput, 0.0)
            """

        else:
            # resets the velocity toggle and time count if shoot is false
            self.velocity_toggle = False
            self.time_count = 0
            # sets the shooter motors to 0 output
            self.r_motor.setVoltage(0)
            self.l_motor.setVoltage(0)

    def shoot_no_PID(self, shoot: bool = False, hold: bool = False, off_time: int = 50, on_time: int = 200):
        """Shooting Function without PID"""
        print(self.position_4dot5.get())

        if shoot:
            # adds 20 to the time count (20 represents the 20ms loop time)
            self.time_count += 20
            self.velocity_toggle = True
            # sets shooter velocity to the setpoint, and the hood to hold the initial angle
            # self.set_velocity(self.setpoint)
            # self.set_hood_angle(0)
            self.master.set(ControlMode.PercentOutput, 0.8)
            # checks if shooter is within 5rpm of the target velocity
            '''
            if self.r_Encoder.getVelocity() >= self.setpoint - 5:
                self.velocity_toggle = True
            '''
            # compares the time count to the off time if the delay_toggle is false
            if self.time_count >= off_time and not self.delay_toggle:
                self.time_count = 0
                self.delay_toggle = True
            # compares the time count to the on time if the delay togle is truw
            elif self.time_count >= on_time and self.delay_toggle:
                self.time_count = 0
                self.delay_toggle = False

            # advances stager until a ball is ready to fire (only occurs if less than four balls are in the system)

            if self.position_4dot5.get():
                self.stager.feeder.set(ControlMode.PercentOutput, .75)
                self.stager.stager.set(ControlMode.PercentOutput, .75)
                self.stager.pre_stager.set(ControlMode.PercentOutput, -.75)
            # checks hold, delay and velocity toggles, if the velocity is good, the delay toggle is true, and hold is false, it will shoot
            elif not hold and self.delay_toggle and self.velocity_toggle:
                # shoot_toggle = True
                self.stager.feeder.set(ControlMode.PercentOutput, 1)
                self.stager.stager.set(ControlMode.PercentOutput, 1)
                self.stager.pre_stager.set(ControlMode.PercentOutput, -1)
            else:
                self.stager.feeder.set(ControlMode.PercentOutput, 0.0)
                self.stager.stager.set(ControlMode.PercentOutput, 0.0)
                self.stager.pre_stager.set(ControlMode.PercentOutput, 0.0)
            # I dont think this is neccessary, will test when I get the chance (goes above the else statement
            """
            elif not self.position_4dot5.get():
                self.stager.feederMotor.set(ControlMode.PercentOutput, 0.0)
                self.stager.stager.set(ControlMode.PercentOutput, 0.0)
                self.stager.pre_stager.set(ControlMode.PercentOutput, 0.0)
            """

        else:
            # resets the velocity toggle and time count if shoot is false
            self.velocity_toggle = False
            self.time_count = 0
            # sets the shooter motors to 0 output
            # self.r_motor.setVoltage(0)
            # self.l_motor.setVoltage(0)
            self.master.set(ControlMode.PercentOutput, 0)

    def set_velocity(self, setpoint: int):
        # converts rpm to ticks per sample period (100ms)
        ticks = self.rpm_to_ticks(setpoint)

        self.master.set(ctre.ControlMode.Velocity, ticks)

    def set_pid(self):
        self.master.config_kF(0, SmartDashboard.getNumber("FF", 0.0))
        self.master.config_kP(0, SmartDashboard.getNumber("P", 0.0))
        self.master.config_kI(0, SmartDashboard.getNumber("I", 0.0))
        self.master.config_kD(0, SmartDashboard.getNumber("D", 0.0))

    def vision_tracking(self):
        visionX = self.limelight.getEntry("tx").getDouble(0)
        self.pan.selectProfileSlot(1, 0)
        self.pan.set(ControlMode.Position, self.pan.getSelectedSensorPosition() - (visionX * (15275 / 180)))
        if abs(self.pan.getClosedLoopError() < 100):
            return True

        return False

    def turret_PID_Test(self):
        self.pan.config_kF(0, SmartDashboard.getNumber("FF", 0.0));
        self.pan.config_kP(0, SmartDashboard.getNumber("P", 0.0));
        self.pan.config_kI(0, SmartDashboard.getNumber("I", 0.0));
        self.pan.config_kD(0, SmartDashboard.getNumber("D", 0.0));

        if self.gamePad.getRawButton(c.buttonA):
            self.pan.set(ControlMode.Position, -1000)
            print(self.pan.getClosedLoopError())

        elif self.gamePad.getRawButton(c.rightBumper):
            self.pan.setSelectedSensorPosition(0)

    def set_hood_angle(self, angle):
        self.hood_angle.set(ControlMode.Position, -angle * (43904 / 27))
        if abs(self.hood_angle.getClosedLoopError()) < 150:
            return True
        return False

    def hood_up_and_down(self, hood_up: bool = False, hood_down: bool = True):
        if hood_up:
            self.hood_piston.set(w.DoubleSolenoid.Value.kForward)
        elif hood_down:
            self.hood_piston.set(w.DoubleSolenoid.Value.kReverse)

    def rpm_to_ticks(self, rpm):
        '''Converts from rpm to ticks per 100ms'''
        return (rpm *2048)/600

    def ticks_to_rpm(self, ticks):
        return (ticks*600)/2048

