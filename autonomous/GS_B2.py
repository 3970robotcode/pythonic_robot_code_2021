import wpilib as w
import traceback
from robot import Robot

class GS_B2():
    def __init__(self, robot: Robot):
        self.robot = robot
        
        w.SmartDashboard.putBoolean("Next State", False)
        self.states = {
            "state1": self.state1,
            "state2": self.state2,
            "state3": self.state3,
            "state4": self.state4,
            "state5": self.state5,
            "state6": self.state6,
            "state7": self.state7,
            "state8": self.state8,
            "state9": self.state9,
            "endState": self.endState
        }
        self.initialState = "state1"
        self.state = self.initialState
        #self.driveTrain = self.robot.drive  
        self.hasStateBeenRun = {
            "state1": False,
            "state2": False,
            "state3": False,
            "state4": False,
            "state5": False,
            "state6": False,
            "state7": False,
            "state8": False,
            "state9": False,
        }

    def reset(self):
        self.state = self.initialState
        self.robot.intake.intakeUpAndDown.set(w.DoubleSolenoid.Value.kForward)
        self.robot.drive.encoderAndGyroReset()

    def state1(self):
        if self.robot.drive.driveToPosition(14):  # and w.SmartDashboard.getBoolean("Next State", False):
            w.SmartDashboard.putBoolean("Next State", False)
            self.robot.drive.encoderAndGyroReset()
            self.state = "state2"

    def state2(self):
        if self.robot.drive.turnToDegree(-20):  # and w.SmartDashboard.getBoolean("Next State", False):
            w.SmartDashboard.putBoolean("Next State", False)
            self.robot.drive.encoderAndGyroReset()
            self.state = "state3"

    def state3(self):
        if self.robot.drive.driveToPosition(161.5):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "state4"

    def state4(self):
        if self.robot.drive.turnToDegree(66.8):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "state5"

    def state5(self):
        if self.robot.drive.driveToPosition(85):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "state6"

    def state6(self):
        if self.robot.drive.turnToDegree(-80):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "state7"


    def state7(self):
        if self.robot.drive.driveToPosition(85):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "state8"

    def state8(self):
        if self.robot.drive.turnToDegree(45):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "state9"

    def state9(self):
        if self.robot.drive.driveToPosition(45):  # and w.SmartDashboard.getBoolean("Next State", False):
            self.robot.drive.encoderAndGyroReset()
            w.SmartDashboard.putBoolean("Next State", False)
            self.state = "endState"

    def endState(self):
        self.robot.drive.stopAllMotors()

    def run(self):
        try:
            print("Current State: " + self.state)
            # self.robot.drive.printMotorOutputs()
            # self.robot.drive.printEncoderAndGyro()
            if self.state == "endState":
                self.robot.stager.stagerMain(False, False, False, False)
            else:
                self.robot.stager.stagerMain(True, False, False, False)
            self.states[self.state]()

        except Exception:
            traceback.print_exc()
