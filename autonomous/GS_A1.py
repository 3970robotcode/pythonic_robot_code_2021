import wpilib as w
import traceback
from robot import Robot

class GS_A1():
    def __init__(self, robot: Robot):
        self.robot = robot
        self.states = {
            "state1": self.state1,
            "state2": self.state2,
            "state3": self.state3,
            "state4": self.state4,
            "state5": self.state5,
            "state6": self.state6,
            "state7": self.state7,
            "endState": self.endState
        }
        
        self.initialState = "state1"
        self.state = self.initialState 
        self.hasStateBeenRun = {
            "state1": False,
            "state2": False,
            "state3": False,
            "state4": False,
            "state5": False,
            "state6": False,
            "state7": False,
        }

    def reset(self):
        self.state = self.initialState
        self.robot.intake.solenoid.set(w.DoubleSolenoid.Value.kForward)
        self.robot.drivetrain.encoderAndGyroReset()

    def state1(self):
        if self.robot.drivetrain.driveToPosition(14):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state2"

    def state2(self):
        if self.robot.drivetrain.turnToDegree(-26.5):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state3"

    def state3(self):
        if self.robot.drivetrain.driveToPosition(67*2):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state4"

    def state4(self):
        if self.robot.drivetrain.turnToDegree(81.8):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state5"

    def state5(self):
        if self.robot.drivetrain.driveToPosition(94.8):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state6"

    def state6(self):
        if self.robot.drivetrain.turnToDegree(-71.5):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state7"

    def state7(self):
        if self.robot.drivetrain.driveToPosition(155):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "endState"

    def endState(self):
        self.robot.drivetrain.stopAllMotors()

    def run(self):
        try:
            print("Current State: " + self.state)
            # self.robot.drivetrain.printMotorOutputs()
            # self.robot.drivetrain.printEncoderAndGyro()
            if self.state == "endState":
                self.robot.stager.main(False, False, False, False)
            else:
                self.robot.stager.main(True, False, False, False)
            self.states[self.state]()

        except Exception:
            traceback.print_exc()
