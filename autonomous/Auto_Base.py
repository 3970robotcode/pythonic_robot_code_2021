import time

class AutoBase():
    def __init__(self):
        self.states = dict()
        self.hasStateBeenRun = dict()
        self.state = ""
        self.start_time = 0

    def onFirstRun(self, *args, usesTiming = False):
        """
        EX:onFirstRun(usesTiming = True)
        EX: onFirstRun(someMethod)
        EX: onFirstRun(someMethod, usesTiming = True)
        Use To Run A Method Or Methods At The Beggining Of A State And Or Set start_time For States Using Timing
        """
        if self.hasStateBeenRun[self.state] == False:
            if usesTiming == True:
                self.start_time = time.time()
            for method in args:
                method()
            self.hasStateBeenRun[self.state] = True
