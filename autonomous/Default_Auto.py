import traceback
import time

from .Auto_Base import AutoBase
from robot import Robot

class defaultAuto(AutoBase):
    def __init__(self, robot: Robot):
        """Init Method for Auto Routine, All Motor and Sensor Reset Commands Or Component Definitions go here"""
        self.robot = robot
        #States Are To Be Put Into This Dict
        self.states = {
                "state1":self.state1, 
                "endState":self.endState
                }
        #Current State
        self.initialState = "state1"
        self.state = self.initialState
        self.start_time = 0
        #Has State Been Run (Important For Timing And The On First Run Method)
        self.hasStateBeenRun = {
                "state1":False,
                "endState":False
                }

    def reset(self):
        self.hasStateBeenRun = {x: False for x in self.hasStateBeenRun}
        self.state = self.initialState

    def state1(self):
        self.onFirstRun(usesTiming = True)
        self.robot.drivetrain.set_output(0.5, 0.5)
        if  time.time() - self.start_time >= 3:
            self.state = "endState"

    def endState(self):
        #End State Should ALways Stop All Motors
        self.robot.drivetrain.stop_all_motors()

    
    def run(self):
        try:
            print("Current State: " + self.state)
            print("Current Time - Start Time: " + str(time.time() - self.start_time))
            print(time.time() - self.start_time >= 3) 
            self.robot.drivetrain.print_motor_outputs()
            self.states[self.state]()
            
        except Exception:
            traceback.print_exc()
