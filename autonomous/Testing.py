import wpilib as w
import ctre

from subsystems.constants import Constants as c
from robot import Robot

class testingAuto():
    def __init__(self, robot: Robot):
        self.robot = robot
        
        self.shiftVar = True
        self.lastValue = False
        self.setpoint = 0

        self.testSelector = w.SendableChooser()
        self.testSelector.setDefaultOption('DT PID', 0)
        self.testSelector.addOption('Gyro PID', 1)
        self.testSelector.addOption('Shifting Test', 2)
        self.testSelector.addOption('Shooter PID', 3)

        w.SmartDashboard.putData("Testing Choices", self.testSelector)
        
        w.SmartDashboard.putBoolean("Reset", False)
        w.SmartDashboard.putNumber("dt_lkP", c.drivetrain_PID["lkP"]) 
        w.SmartDashboard.putNumber("dt_lkI", c.drivetrain_PID["lkI"])
        w.SmartDashboard.putNumber("dt_lkD", c.drivetrain_PID["lkD"])
        
        w.SmartDashboard.putNumber("dt_rkP", c.drivetrain_PID["rkP"])
        w.SmartDashboard.putNumber("dt_rkI", c.drivetrain_PID["rkI"])
        w.SmartDashboard.putNumber("dt_rkD", c.drivetrain_PID["rkD"])  
        
        w.SmartDashboard.putNumber("shooter_rkP", c.shooter_PID["rkP"])
        w.SmartDashboard.putNumber("shooter_rkI", c.shooter_PID["rkI"])
        w.SmartDashboard.putNumber("shooter_rkD", c.shooter_PID["rkD"])
        w.SmartDashboard.putNumber("shooter_rkF", c.shooter_PID["rkF"])
        w.SmartDashboard.putNumber("shooter_rkIZone", c.shooter_PID["rkIZone"])

        w.SmartDashboard.putNumber("setpoint", self.setpoint)

    def reset(self):
        self.robot.drivetrain.encoder_and_gyro_reset()
        self.robot.drivetrain.l_master.set(ctre.ControlMode.PercentOutput, 0)
        self.robot.drivetrain.r_master.set(ctre.ControlMode.PercentOutput, 0) 
        self.robot.shooter.master.set(ctre.ControlMode.PercentOutput, 0)

    def run(self):
        
        if self.testSelector.getSelected() == 0:
            if w.SmartDashboard.getBoolean("Reset", False):

                self.robot.drivetrain.stopAllMotors()
                self.robot.drivetrain.l_master.config_kP(0, w.SmartDashboard.getNumber('lkP', 0))
                self.robot.drivetrain.l_master.config_kI(0, w.SmartDashboard.getNumber('lkI', 0))
                self.robot.drivetrain.l_master.config_kD(0, w.SmartDashboard.getNumber('lkD', 0))

                self.robot.drivetrain.r_master.config_kP(0, w.SmartDashboard.getNumber('rkP', 0))
                self.robot.drivetrain.r_master.config_kI(0, w.SmartDashboard.getNumber('rkI', 0))
                self.robot.drivetrain.r_master.config_kD(0, w.SmartDashboard.getNumber('rkD', 0))

                self.setpoint = w.SmartDashboard.getNumber("setpoint", 0)

                self.robot.drivetrain.encoder_and_gyro_reset()

            else:
                self.robot.drivetrain.driveToPosition(self.setpoint)
                print("running")
                print("seetpoint"+str(self.setpoint))

        elif self.testSelector.getSelected() == 2:
            if self.shiftVar and not self.lastValue == w.SmartDashboard.getBoolean("shift", False):
                self.robot.drivetrain.driveShift.set(w.DoubleSolenoid.Value.kForward)
                self.shiftVar = False
                self.lastValue = w.SmartDashboard.getBoolean("shift", False)
            elif not self.shiftVar and not self.lastValue == w.SmartDashboard.getBoolean("shift", False):
                self.robot.drivetrain.driveShift.set(w.DoubleSolenoid.Value.kReverse)
                self.shiftVar = True
                self.lastValue = w.SmartDashboard.getBoolean("shift", False)

        elif self.testSelector.getSelected() == 1:
            # code to test turn to angle function
            print(self.robot.drivetrain.turnToDegree(-90.0))
            self.robot.drivetrain.printEncoderAndGyro()
            w.SmartDashboard.putNumber("Gyro", self.robot.drivetrain.driveTrainGyro.getAngle())
            if w.SmartDashboard.getBoolean("Reset", False):
                self.robot.drivetrain.driveTrainGyro.reset()

        elif self.testSelector.getSelected() == 3: 
            w.SmartDashboard.putNumber("shooter rpm", self.robot.shooter.ticks_to_rpm(self.robot.shooter.master.getSelectedSensorVelocity()))
            if w.SmartDashboard.getBoolean("Reset", False):

                self.robot.shooter.master.set(ctre.ControlMode.PercentOutput, 0)
                self.robot.shooter.master.config_kP(0, w.SmartDashboard.getNumber("shooter_rkP", 0))
                self.robot.shooter.master.config_kI(0, w.SmartDashboard.getNumber("shooter_rkI", 0))
                self.robot.shooter.master.config_kD(0, w.SmartDashboard.getNumber("shooter_rkD", 0))
                self.robot.shooter.master.config_kF(0, w.SmartDashboard.getNumber("shooter_rkF", 0))
                self.robot.shooter.master.config_IntegralZone(0, w.SmartDashboard.getNumber("shooter_rkIZone", 0))


                self.setpoint = self.robot.shooter.rpm_to_ticks(w.SmartDashboard.getNumber("setpoint", 0))

            else:
                self.robot.shooter.set_velocity(self.setpoint)
