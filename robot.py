import traceback
import time

import wpilib as w
import ctre

#Component, Autonomous, and Controller Import Statements
#from components import *
#from autonomous import *
import subsystems as s

import autonomous as a

#Constants File For Motor IDs, Joystick IDs, Etc...
from subsystems import Constants as c

class Robot(w.TimedRobot):
    """FRC Robot"""
    def robotInit(self):
        """Robot-wide initialization code should go here"""
        self.l_stick = w.Joystick(c.r_stick)
        self.r_stick = w.Joystick(c.l_stick)
        
        self.gamepad = w.Joystick(c.gamepad)
        
        self.drivetrain = s.Drivetrain()
        self.intake = s.Intake()
        self.stager = s.Stager(self.intake)
        self.shooter = s.Shooter(self.stager)

        self.autonomous_modes = {
                0:a.defaultAuto(self),
                1:a.testingAuto(self),
                2:a.GS_A1(self),
                3:a.GS_A2(self),
                4:a.GS_B1(self),
                5:a.GS_B2(self)
                }
        
        #Sendable Chooser For Auto Modes
        self.chooser = w.SendableChooser()
        self.chooser.setDefaultOption('Default Auto (Go Straight)', 0)
        self.chooser.addOption('Testing', 1)
        self.chooser.addOption('GS A1', 2)
        self.chooser.addOption('GS A2', 3)
        self.chooser.addOption('GS B1', 4)
        self.chooser.addOption('GS B2', 5)

        w.SmartDashboard.putData('Autonomous Mode Selector', self.chooser)

        #Shooter Variable
        self.up_down = True
        
        #Compressor Code
        airCompressor = w.Compressor(0)
        airCompressor.setClosedLoopControl(True)
        airCompressor.start()

    def robotPeriodic(self):
        """Called Every 20ms Regardless of Mode"""
        pass

    def disabledInit(self):
        """Called only at the beginning of disabled mode"""
        #sets the drivetrain motors to break mode to halt robot travel and then to coast mode 
        self.drivetrain.encoder_and_gyro_reset()
        
        self.drivetrain.l_master.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.l_slave.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.r_master.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.r_slave.setNeutralMode(ctre.NeutralMode.Brake)
        
        time.sleep(0.5)
       
        self.drivetrain.l_master.setNeutralMode(ctre.NeutralMode.Coast)
        self.drivetrain.l_slave.setNeutralMode(ctre.NeutralMode.Coast)
        self.drivetrain.r_master.setNeutralMode(ctre.NeutralMode.Coast)
        self.drivetrain.r_slave.setNeutralMode(ctre.NeutralMode.Coast)

    def disabledPeriodic(self):
        """Called every 20ms in disabled mode"""
        pass
        
    def autonomousInit(self):
        """Called only at the beginning of autonomous mode"""
        self.mode = self.chooser.getSelected()
        self.autonomous_modes[self.mode].reset()
        
    def autonomousPeriodic(self):
        """Called every 20ms in autonomous mode"""
        self.autonomous_modes[self.mode].run()

    def teleopInit(self):
        """Called only at the beginning of teleoperated mode"""
        self.stager.pre_stager_up()
        pass

    def teleopPeriodic(self):
        """Called every 20ms in teleoperated mode"""
        try:
            #set control variables based on user input
            shift = self.l_stick.getRawButton(6)

            intake = self.gamepad.getRawButton(c.gp["b"]) 
            outake = self.gamepad.getRawButton(c.gp["back"]) 
            push = self.gamepad.getRawButton(c.gp["start"]) 
            shoot = self.gamepad.getRawButton(c.gp["a"]) 
            hold = self.gamepad.getRawButton(c.gp["r_bumper"])
            raise_lower = self.gamepad.getRawButtonPressed(c.gp["l_bumper"])

            raise_intake = self.gamepad.getRawButton(c.gp["y"])
            lower_intake = self.gamepad.getRawButton(c.gp["x"])
            
            #Should this be changed so that the getY's set a control variable, or should that be reserved for buttons?
            #Could also be used only whenever it makes things more clear\when you have more than one function that uses that input.
            
            self.drivetrain.set_output(self.l_stick.getY(), self.r_stick.getY())
            self.drivetrain.shift(shift)

            self.stager.main(intake, outake, push, shoot)
           
            self.shooter.shoot_no_PID(shoot, hold)

            if raise_lower:
                self.up_down = not self.up_down
            self.shooter.hood_up_and_down(self.up_down)
            
            self.intake.raise_and_lower(raise_intake, lower_intake)
     
        except Exception:
            traceback.print_exc()

    def testInit(self):
        """Called only at the beginning of test mode"""
        pass

    def testPeriodic(self):
        """Called every 20ms in test mode"""
        pass
 


if __name__ == "__main__":
    w.run(Robot)
 
